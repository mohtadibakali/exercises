<?php


 require_once ('Validator.php');

class Movies
{

    private $title;
    private $year;
    private $director;
    private $mpaa_rating;

    function __construct($_title,$_year,$_director,$_mpaa_rating){
        $this->title = $_title;
        $this->year = $_year;
        $this->director = $_director;
        $this->mpaa_rating = $_mpaa_rating;
    }


    public function getTitle(){
        return $this->title;
    }

    public function getRating(){
        return $this->mpaa_rating;
    }

    public function getDirector(){
        return $this->director;

    }
    public function getMpaaRating(){
        return $this->mpaa_rating;
    }


}