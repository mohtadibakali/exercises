<?php
 define ('user_input', 1);
define ('email', 2);
define ('integer', 3);
define ('url', 4);
define ('telephone', 5);


class Validator
{
    public $data_types = array('user_input' => user_input,'email' => email ,'integer' => integer,'url' => url,'telephone' => telephone);
    public $error;
    public function checkType($type,$input){

        if(array_search($type,$this->data_types)){

           switch ($type){
               case 1:
                    return $this->sanitizeUserInputText($input);
                    break;
               case 2:
                   return $this->sanitizeUserEmail($input);
                   break;
               case 3:
                   return $this->sanitizeInteger($input);
                   break;
               case 4:
                   return $this->sanitizeUrl($input);
                   break;
               case 5:
                   print "this is a telephone type";
                   break;
           }
        }else {
            $this->error = 'no existing type';
        }

    }

    public function sanitizeUserInputText($input){
         $input = filter_var($input, FILTER_SANITIZE_STRING);
         return $input;
     }

    public function sanitizeUserEmail($input){
        $input = filter_var($input, FILTER_SANITIZE_EMAIL);
        return $input;
    }
    public function sanitizeInteger($input){
        $input = filter_var($input, FILTER_SANITIZE_NUMBER_INT);
        return $input;
    }

    public function sanitizeUrl($input){
        $input = filter_var($input, FILTER_SANITIZE_URL);
        return $input;
    }


}