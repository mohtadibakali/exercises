<?php
require_once ('Persona.php');

/**
 * Created by PhpStorm.
 * User: mohtadi
 * Date: 26/01/17
 * Time: 18:38
 */
class Escuela
{
    private $_nombreEscuela;
    private $_alumnos = array();

    function __construct($escuelaNombre){

        $this->_nombreEscuela = $escuelaNombre;
    }

    public function addNewAlumno(Persona $persona){
        $this->_alumnos []= $persona;
    }

    public function getAlunnos(){
        return $this->_alumnos;
    }

    public function getEscuelanombre(){
        return $this->_nombreEscuela;
    }



}