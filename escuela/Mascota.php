<?php
require_once ('Persona.php');

/**
 * Created by PhpStorm.
 * User: mohtadi
 * Date: 26/01/17
 * Time: 18:38
 */
class Mascota
{
    private $_nombre;
    private $_animal;
    private $_reaccion;
    private $_persona;


    function __construct(Persona $persona, $nombre,$animal)
    {
        $this->_nombre = $nombre;
        $this->_animal = $animal;
        $this->_persona = $persona;
    }

    function  getNombreMascota(){
        return $this->_nombre;
    }

    function  getMascotaTipo(){
        return $this->_animal;
    }

    function getOwner(){
        return $this->_persona;
    }





}