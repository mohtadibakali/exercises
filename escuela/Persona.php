<?php
require_once ('Mascota.php');

/**
 * Created by PhpStorm.
 * User: mohtadi
 * Date: 26/01/17
 * Time: 18:36
 */
class Persona
{
    private  $_nombre;
    private  $_fechaNacimiento;
    private  $_amigos = array();
    private  $_hermanos = array();
    private  $_mascota;

    function __construct($nombre,$fechaNacimento)
    {
        $this->_nombre = $nombre;
        $this->_fechaNacimiento = $fechaNacimento;
    }

    function  getNombre(){
        return $this->_nombre;
    }

    function addHermanos($nombreHermano){
        $this->_hermanos [] = $nombreHermano;
    }

    function getHermanos(){
        return $this->_hermanos;
    }

    function addAmigos($amiguito){
        $this->_amigos [] = $amiguito;
    }

    function getAmigos(){
        return $this->_amigos;
    }

    function addMascota(Mascota $m){
            $this->_mascota = $m;
    }
    function getMascotaDetaills(){
        return $this->_mascota->getNombreMascota();
    }


    function __toString()
    {
        return $this->_nombre;
    }

    function toString(){
	    return $this->_nombre;
     }


}
