<?php

/**
 * Created by PhpStorm.
 * User: mohtadi
 * Date: 29/01/17
 * Time: 19:37
 */

require_once ('Alumno.php');
require_once('Persona.php');
require_once ('CochesEscolares.php');


class Escuela
{

    private $_name;
    private $_address;
    private $_alumnos = array();
    private $_persona = array();
    private $_cochesEscolares = array();

    function __construct($name,$address){
            $this->_name = $name;
            $this->_address = $address;
    }

    public function getSchoolName(){
        return $this->_name;
    }

    public function getAddress(){
        return $this->_address;
    }

    public function addNewStudent( Alumno $alumno ){
        $this->_alumnos [] = $alumno;
    }

    public function addNewPersona (Persona $persona){
        $this->_persona []= $persona;
    }

    public  function addNewCoche(CochesEscolares $newCoche){
         $this->_cochesEscolares [] = $newCoche;
    }


    public function __toString()
    {
         return "Scool Name :" . $this->_name ."<br>". " Address : ".$this->_address;
    }


    public function getAlumnos(){
        return $this->_alumnos;
    }


}


