<?php

require_once ('Persona.php');
class Usuario extends Persona
{

    public $id_usuario;
    protected static $contador = 0;

    function __construct($nombre, $apellidos, $fecha_nacimiento,$idusuario) {
         parent::__construct($nombre,$apellidos,$fecha_nacimiento);
         $this->id_usuario = self::$contador+=1;
    }

    public function __toString() {
        return $this->id_usuario .'  '. parent::__toString();
    }


}