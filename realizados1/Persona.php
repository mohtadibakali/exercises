<?php

/**
 * Created by PhpStorm.
 * User: mohtadi
 * Date: 30/01/17
 * Time: 16:39
 */
class Persona
{
    private $_nombre;
    private $_apellidos;
    private $_fecha_nacimiento;

    function __construct($nombre,$apellidos, $fecha_nacimiento)
    {
        $this->_nombre = $nombre;
        $this->_apellidos = $apellidos;
        $this->_fechaNacimiento = $fecha_nacimiento;
    }

    function __toString()
    {
       return $this->_nombre.' '.$this->_apellidos.' '.$this->_fechaNacimiento;
    }


}